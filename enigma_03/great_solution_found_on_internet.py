import re
from typing import List
from math import prod

'''
    Failure to find a proper answer for this enigma_03
    The solution proposed here comes from
        https://www.reddit.com/r/adventofcode/comments/189m3qw/comment/kbs9g3g/?utm_source=share&utm_medium=web3x&utm_name=web3xcss&utm_term=1&utm_content=share_button
        https://topaz.github.io/paste/#XQAAAQAcAgAAAAAAAAA0m0pnuFI8c+fPp4G3Y5M2miSs3R6AnrKm3fbDkugpdVsCgQOTZL/yzxGwy/N08UeBslxE7G36XluuSq4Y/2FE0+4nPcdj9lpkrrjBk5HRCFLEKuPjUV8tYPx04VDoJ1c6yyLzScmAGwNvzpPoqb5PkRyyy4dSEcuEDe/k0/U7h7pZVh4eTrNAIPsTNZohcltxuwuA4lrZSN37i0QZiufFpvLVyhV/dLBnmSr+2jwFcFE+W6OEIFQxK6MIJ2z7TWKj8lg6yV4yhJzTm+c+QHh2omzhGVLd2WdcHdhjmCyC+Btbr3yCqemYb/6tMUvz8VchnyHstx7QKKeLVmTOEyYqHH/qRDhlKXSQ23RWuPibCf4quQUPGpPDRsH4KITzLbIUVUdssnSp6ffcHO+dAISdzBOiznl5/+PI+jE=

'''



# FIRST PART
def first_part(input_list: List[str]):

    # generates dictionary
    #   key (row, col)  coordinates of any found chars
    #   value []        (empty list)
    chars = {
            (row,col): []
            for row in range(len(input_list))
            for col in range(len(input_list))
            if input_list[row][col] not in '0123456789.'
         }

    # for each line of file
    for idx, row in enumerate(input_list):
        #for each number in line
        for n in re.finditer(r'\d+', row):

            # edge_coordinates stores all 'edge' coordinates for the curent number n
            edge_coordinates = {
                (x, y)
                for x in (idx - 1, idx, idx + 1)
                for y in range(n.start() - 1, n.end() + 1)
            }
            for matched in edge_coordinates & chars.keys():
                chars[matched].append(int(n.group()))

    print(sum(sum(i) for i in chars.values()))
    print(sum(prod(p) for p in chars.values() if len(p)==2))



if "__main__" in __name__:
    with open("input.txt", 'r') as f:
        input = f.readlines()
    first_part(input)
    # second_part(input)
