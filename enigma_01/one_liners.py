from typing import List



# FIRST PART
def first_part(input_list) -> () :
    '''
        The global list is generated with 'for value in input_list' expression

        Then we generate a "sub list" with each char that can be cast/converted to digit/number
            expression:     [i for i in value if i.isdigit()]

        This "sub list" is assigned to variable 'x' with the ":=" operator
            expression:     (x := [i for i in value if i.isdigit()])

        At this point we have a list of list of string contained in variable 'x'
            exemple:        x variable will contain [['1','2','3','4'],['9'],['4','0','1']]

        For each entry of x, we concatenate the first (index [0]) and last element (index [-1]), then cast/convert as int
            expression:     int(str((x := [i for i in value if i.isdigit()])[0] + x[-1])
            exemple:        [['1','2','3','4'],['9'],['4','0','1']] will transform as [14,99,41]

        then we make the sum() of all integer contained in the list
            expression:     sum(int(str((x := [i for i in value if i.isdigit()])[0] + x[-1])) for value in input_list)
    '''

    final_result = sum(int(str((x := [i for i in value if i.isdigit()])[0] + x[-1])) for value in input_list)
    print(final_result)



# SECOND PART
def second_part(input_list: List[str]):
    '''
        The global list is generated with 'for value in input_list' expression

        Then we iterate over each char of value (with the char index as i)
            expression:     for i in range(len(value))

        For each char of value, we check if any entry of 'number_as_word' is in value
        exemple:            if "five" is in value, then it will transform as "5ve"
        expression:         x if (x:= "".join([str(idx) for idx, word in enumerate(number_as_word, 1) if value[i:].startswith(word)])) else value[i]

        Details:
            global expression:     [str(idx) for idx, word in enumerate(number_as_word, 1) if value[i:].startswith(word)]
            generates a list as:    for each entry of number_as_word, if this entry is in value, append str(idx) to list

        Then the value is assigned to 'x' variable with expression      x if ( ... )
        If no value could be assigned to 'x', then 'x' is assigned the current char with expression     else value[i]

        At the end, r_list contains a list of list of char, which is almost the same as a list of list of string
    '''

    number_as_word = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
    r_list = [[x if (x:= "".join([str(idx) for idx, word in enumerate(number_as_word, 1) if value[i:].startswith(word)])) else value[i] for i in range(len(value))] for value in input_list]
    return r_list




if "__main__" in __name__:
    with open("input.txt", 'r') as f:
        input = f.readlines()
    first_part(second_part(input))
