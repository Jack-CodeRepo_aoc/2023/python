from typing import List



# FIRST PART
def first_part(input_list):
    r_list = []
    # for each values in given input_list
    for value in input_list:
        tmp_list = []
        # iterate over each char
        for i in value:
            # if current chat can be cast/converted to a number (digit)
            #   append to tmp_list
            if i.isdigit():
                tmp_list.append(i)
        r_list.append(tmp_list)

    # at this point, r_list contains a list of list of number
    # each list of number contains all number contained in one value

    final_result = int()
    for idx, number in enumerate(r_list):
        # generates the calibration with first number and last number
        final_number = str(number[0]+number[-1])
        final_result += int(final_number)

    print(final_result)



# SECOND PART
def second_part(input_list: List[str]):
    number_as_word = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]

    r_list = []
    for value in input_list:
        w_list = []

        # for each letter in parsed value
        for i in range(len(value)):
            tmp_list = []

            # for each word in number_as_word
            for idx, word in enumerate(number_as_word, 1):
                # if the parse value[letter:] has the target word, add idx as string to tmpl list
                # idx will be 1,2,3 ... up to max length of value
                # valid exemple:   ghjone3three
                #   will match when i == 4 with one3three since it starts with "one". this will add "1" to tmp_list (and not 1)
                #   will match when i == 8 with three since it starts with "three". this will add "3" to tmp_list (and not 3)
                if value[i:].startswith(word):
                    tmp_list.append(str(idx))

            # this will append each char to w_list
            if len(tmp_list) >= 1:
                w_list.append("".join(tmp_list))
            else: w_list.append(value[i])

        r_list.append("".join(w_list))

    return r_list




if "__main__" in __name__:
    with open("input.txt", 'r') as f:
        input = f.readlines()
    first_part(second_part(input))

