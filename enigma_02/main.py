
from typing import List


class RGB:
    def __init__(self, liste: List):
        if len(liste) == 3:
            self.red = 0 if liste[0] is None else liste[0]
            self.green = 0 if liste[1] is None else liste[1]
            self.blue = 0 if liste[2] is None else liste[2]
        else: print("ERROR", liste)

    def __repr__(self):
        return f"[{self.red}, {self.green}, {self.blue}]"

    def power(self):
        return self.red * self.green * self.blue

class Game:
    def __init__(self, line):
        self.id = int(line.split(":")[0].split(" ")[1])
        self.scores = self.generate_scores(line)

    def compare_with_bag(self, bag: RGB):
        legit_ids = 0
        for score in self.scores:
            if score.red <= bag.red and score.green <= bag.green and score.blue <= bag.blue:
                legit_ids += 1

        if len(self.scores) == legit_ids:
            return self.id
        else: return 0



    @staticmethod
    def generate_scores(line):
        r_list = []
        for entry in line.split(":")[1].split(";"):
            col_list = [None] * 3

            for e in [l.strip() for l in entry.strip().split(",")]:
                num = int(e.split(" ")[0])
                col = e.split(" ")[1]
                match col:
                    case "red": col_list[0] = num
                    case "green": col_list[1] = num
                    case "blue": col_list[2] = num
                    case _: pass
            r_list.append(RGB(col_list))
        return r_list

    def generate_power(self):
        power = RGB([0, 0, 0])
        for score in self.scores:
            if score.red > power.red:
                power.red = score.red
            if score.green > power.green:
                power.green = score.green
            if score.blue > power.blue:
                power.blue = score.blue

        return power.power()


# FIRST PART
def first_part(input_list) -> () :
    '''
    This also works
        print(sum([Game(game).compare_with_bag(RGB([12, 13, 14])) for game in input_list]))
    '''
    BAG = RGB([12, 13, 14])
    print(sum([Game(game).compare_with_bag(BAG) for game in input_list]))





# SECOND PART
def second_part(input_list):
    print(sum([Game(game).generate_power() for game in input_list]))



if "__main__" in __name__:

    with open("input.txt", 'r') as f:
        input = f.readlines()
    first_part(input)
    second_part(input)