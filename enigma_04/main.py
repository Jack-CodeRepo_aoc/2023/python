
import re
from typing import List


def first_part(input_list: List[str]):
    r = []
    points_sum = 0
    for idx, line in enumerate(input_list):

        wins = [i for i in line.strip().split(":")[1].split("|")[0].split(" ") if i.isdigit()]
        nums = [i for i in line.strip().split(":")[1].split("|")[1].split(" ") if i.isdigit()]
        win_win = len([ i for i in nums if i in wins ])

        points_sum += int(pow(2, win_win-1))


    print(points_sum)


def second_part(input_list: List[str]):
    cards_number = { i:1 for i in range(len(input_list))}

    for idx, line in enumerate(input_list):
            wins = [i for i in line.strip().split(":")[1].split("|")[0].split(" ") if i.isdigit()]
            nums = [i for i in line.strip().split(":")[1].split("|")[1].split(" ") if i.isdigit()]
            win_win = len([i for i in nums if i in wins])
            print(idx, win_win)
            for r in range(idx + 1, idx + win_win + 1):
                cards_number[r] += 1 * cards_number[idx]
    print(sum(cards_number.values()))




if "__main__" in __name__:
    with open("input.txt", 'r') as f:
        input = f.readlines()
    first_part(input)
    second_part(input)